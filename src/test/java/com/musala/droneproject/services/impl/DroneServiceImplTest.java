package com.musala.droneproject.services.impl;

import com.musala.droneproject.models.dtos.requests.DroneRequest;
import com.musala.droneproject.models.dtos.requests.LoadBatchRequest;
import com.musala.droneproject.models.dtos.requests.MedicationItemRequest;
import com.musala.droneproject.models.dtos.responses.DroneResponse;
import com.musala.droneproject.models.dtos.responses.MedicationResponse;
import com.musala.droneproject.models.entities.Drone;
import com.musala.droneproject.models.entities.DroneModel;
import com.musala.droneproject.models.entities.DroneState;
import com.musala.droneproject.repositories.DroneRepository;
import com.musala.droneproject.repositories.LoadBatchRepository;
import com.musala.droneproject.services.MedicationService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class DroneServiceImplTest {

    @InjectMocks
    private DroneServiceImpl service;

    @Mock
    private DroneRepository droneRepository;

    @Mock
    private MedicationService medicationService;

    @Mock
    private LoadBatchRepository loadBatchRepository;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void register() throws Exception {
        DroneRequest request = new DroneRequest();
        request.setBatteryCapacity(45.4);
        request.setState(DroneState.IDLE);
        request.setModel(DroneModel.Lightweight);
        request.setSerialNumber(String.valueOf(UUID.randomUUID()));

        Drone result =new Drone();
        result.setBatteryCapacity(45.4);
        result.setState(DroneState.IDLE);
        result.setModel(DroneModel.Lightweight);

        when(droneRepository.save(any())).thenReturn(result);

        DroneResponse response =   service.register(request);

        assertEquals(response.getBatteryCapacity(),result.getBatteryCapacity());
    }

    @Test
    void loadThrowExecptionOnSerial() {
        LoadBatchRequest request = new LoadBatchRequest();
        try {

            request.setDroneSerial(String.valueOf(UUID.randomUUID()));

            when(droneRepository.findBySerialNumber(any())).thenReturn(Optional.empty());
            service.load(request);
        }
        catch(RuntimeException er){
            String errorMessage = format("Drone with serial %s does   Not  Exist",request.getDroneSerial());
            assertEquals(er.getMessage(),errorMessage);
        }
    }


    @Test
    void loadThrowExecptionOnDronAvaiability() {
        LoadBatchRequest request = new LoadBatchRequest();
        Drone drone =  getDrone(25d,DroneState.LOADED);
        drone.setBatteryCapacity(55.5);
        try {

            request.setDroneSerial(String.valueOf(UUID.randomUUID()));


            when(droneRepository.findBySerialNumber(any())).thenReturn(Optional.of(drone));
            service.load(request);
        }
        catch(RuntimeException er){
            String errorMessage = format("Drone is not available for loading it is currently %s ", drone.getState());
            assertEquals(er.getMessage(),errorMessage);
        }
    }

    @Test
    void loadThrowExecptionOnDronLowPower() {
        LoadBatchRequest request = new LoadBatchRequest();
        Drone drone =  getDrone(25d,DroneState.IDLE);
        drone.setBatteryCapacity(15.5);
        try {

            request.setDroneSerial(String.valueOf(UUID.randomUUID()));
            when(droneRepository.findBySerialNumber(any())).thenReturn(Optional.of(drone));
            service.load(request);
        }
        catch(RuntimeException er){
            String errorMessage = format("Drone  does not have enough power ,it is current %s ", drone.getBatteryCapacity());
            assertEquals(er.getMessage(),errorMessage);
        }
    }


    @Test
    void loadThrowExecptionOnDronNoItems() {
        LoadBatchRequest request = new LoadBatchRequest();
        Drone drone =  getDrone(25d,DroneState.IDLE);
        drone.setBatteryCapacity(25.5);
        try {

            request.setDroneSerial(String.valueOf(UUID.randomUUID()));
            when(droneRepository.findBySerialNumber(any())).thenReturn(Optional.of(drone));
            service.load(request);
        }
        catch(RuntimeException er){
            String errorMessage = format("batch has  no items added");
            assertEquals(er.getMessage(),errorMessage);
        }
    }

    @Test
    void loadThrowExecptionOnDroneSomeItemsDontExist() {
        LoadBatchRequest request = new LoadBatchRequest();
        Drone drone =  getDrone(25d,DroneState.IDLE);
        drone.setBatteryCapacity(25.5);
        drone.setWeightLimit(40.2);

        MedicationItemRequest xt = new MedicationItemRequest();
        xt.setCode("code");
        List<MedicationItemRequest> items  = new ArrayList<>();
        items.add(xt);
        items.add(xt);
        request.setItems(items);
        try {

            request.setDroneSerial(String.valueOf(UUID.randomUUID()));
            when(droneRepository.findBySerialNumber(any())).thenReturn(Optional.of(drone));

            List<MedicationResponse>  medicationResponses =  new ArrayList<>();
            MedicationResponse response =new MedicationResponse();
            response.setId(1);
            response.setWeight(23);
            medicationResponses.add(response);
            when(medicationService.findAllByCode(any())).thenReturn(medicationResponses);
            service.load(request);
        }
        catch(RuntimeException er){
            String errorMessage = format("Batch Items Added  do not  exist");
            assertEquals(er.getMessage(),errorMessage);
        }
    }

    @Test
    void loadThrowExecptionOnDroneLoadCapacity() {
        LoadBatchRequest request = new LoadBatchRequest();
        Drone drone =  getDrone(25d,DroneState.IDLE);
        drone.setBatteryCapacity(25.5);
        drone.setWeightLimit(40.2);

        MedicationItemRequest xt = new MedicationItemRequest();
        xt.setCode("code");
        List<MedicationItemRequest> items  = new ArrayList<>();
        items.add(xt);
        items.add(xt);
        request.setItems(items);
        try {

            request.setDroneSerial(String.valueOf(UUID.randomUUID()));
            when(droneRepository.findBySerialNumber(any())).thenReturn(Optional.of(drone));

            List<MedicationResponse>  medicationResponses =  new ArrayList<>();
            MedicationResponse response =new MedicationResponse();
            response.setId(1);
            response.setWeight(23);
            medicationResponses.add(response);
            medicationResponses.add(response);
            when(medicationService.findAllByCode(any())).thenReturn(medicationResponses);
            service.load(request);
        }
        catch(RuntimeException er){
            String errorMessage = format("Total Load  is greater than load capacity" );
            assertEquals(er.getMessage(),errorMessage);
        }
    }


    @Test
    @DisplayName("Validate drone Power below capacity")
    void isDronePowerEnoughNotEnouguh() {
        Drone drone =  new Drone();
        drone.setBatteryCapacity(10d);
         boolean result =    service.isDronePowerEnough(drone);
         assertEquals(result,false);
    }

    @Test
    @DisplayName("Validate drone Power ")
    void isDronePowerEnough() {
        Drone drone =  new Drone();
        drone.setBatteryCapacity(25d);
        boolean result =    service.isDronePowerEnough(drone);
        assertEquals(result,true);
    }


    @Test
    @DisplayName("Validate drone availability ")
    void isDronnAvailable_Unvailable() {
        Drone drone =  new Drone();
        drone.setBatteryCapacity(25d);
        drone.setState(DroneState.IDLE);
        boolean result =    service.isDronnAvailable(drone);
        assertEquals(result,true);
    }


    @Test
    @DisplayName("Validate drone availability ")
    void isDronnAvailable() {
        Drone drone =  new Drone();
        drone.setBatteryCapacity(25d);
        drone.setState(DroneState.LOADING);
        boolean result =    service.isDronnAvailable(drone);
        assertEquals(result,false);

        drone.setState(DroneState.LOADED);
          result =    service.isDronnAvailable(drone);
        assertEquals(result,false);


        drone.setState(DroneState.DELIVERING);
          result =    service.isDronnAvailable(drone);
        assertEquals(result,false);

        drone.setState(DroneState.RETURNING);
          result =    service.isDronnAvailable(drone);
        assertEquals(result,false);

    }

    @Test
    void checkBattery() {
        Drone drone =  getDrone(25d,DroneState.LOADED);
        drone.setBatteryCapacity(55.5);
        when(droneRepository.findBySerialNumber(any())).thenReturn(Optional.of(drone));
       DroneResponse result = service.checkBattery("serial");
       assertEquals(result.getBatteryCapacity(),drone.getBatteryCapacity());

    }
    @Test
    void checkAvailableDrones() {

        List<Drone>  drones =new ArrayList<>();
        drones.add(getDrone(25d,DroneState.LOADED));
        drones.add(getDrone(25d,DroneState.IDLE));
        drones.add(getDrone(25d,DroneState.IDLE));
        drones.add(getDrone(25d,DroneState.LOADING));
        drones.add(getDrone(25d,DroneState.RETURNING));
        drones.add(getDrone(25d,DroneState.DELIVERING));
        drones.add(getDrone(25d,DroneState.DELIVERED));
        drones.add(getDrone(25d,DroneState.IDLE));

        when(droneRepository.findAll()).thenReturn(drones);
      List<DroneResponse> result =  service.checkAvailableDrones();
      assertEquals(result.size(),3);
    }

    private static Drone getDrone(double capacity,DroneState  state ) {
        Drone drone = new Drone();
        drone.setBatteryCapacity(capacity);
        drone.setState(state);
        return drone;
    }


}