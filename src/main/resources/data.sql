-- insert somme medications
insert into medications(name,weight,code,image) values (
'(PFR) 1G IVIM',25,'ATHC3',''
);

insert into medications(name,weight,code,image) values (
'chloramphenicol',25,'DMHC4',''
);


insert into medications(name,weight,code,image) values (
'chloramphenicol injection (PFR) 1G IVIM',25,'CTHC3',''
);

--insert some drones
insert into drones(serialnumber,model,weight_limit,battery_capacity,state) values (
'001','Lightweight',56,67.8,'IDLE'
);

insert into drones(serialnumber,model,weight_limit,battery_capacity,state) values (
'002','Lightweight',56,67.8,'DELIVERING'
);

insert into drones(serialnumber,model,weight_limit,battery_capacity,state) values (
'003','Cruiserweight',56,67.8,'LOADED'
);
