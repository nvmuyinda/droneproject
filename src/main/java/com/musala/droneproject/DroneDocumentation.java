package com.musala.droneproject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Configuration
@EnableSwagger2
public class DroneDocumentation {

    public static final String APPLICATION_JSON = "application/json";
    private static final Set<String> consumes = new HashSet<>(Collections.singleton(APPLICATION_JSON));
    private static final Set<String> produces = new HashSet<>(Collections.singleton(APPLICATION_JSON));

    @Bean
    public Docket api(){
        return  new Docket(DocumentationType.SWAGGER_2)
                .consumes(consumes)
                .produces(produces)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.musala.droneproject.controllers"))
                .paths(PathSelectors.any())
                .build();
    }

}
