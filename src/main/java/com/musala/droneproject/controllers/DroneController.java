package com.musala.droneproject.controllers;


import com.musala.droneproject.models.dtos.requests.DroneRequest;
import com.musala.droneproject.models.dtos.requests.LoadBatchRequest;
import com.musala.droneproject.models.dtos.responses.DroneResponse;
import com.musala.droneproject.models.dtos.responses.LoadResponse;
import com.musala.droneproject.services.DroneService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("v1/drone")
@Validated
@Slf4j
public class DroneController extends BaseController {

    private DroneService service;

    @Autowired
    public DroneController(DroneService service) {
        this.service = service;
    }

    @PostMapping("/register")
    public ResponseEntity<DroneResponse> register(@Valid @RequestBody DroneRequest request) throws RuntimeException {
          return  ResponseEntity.ok(service.register(request));
    }



    @PostMapping("/load")
    public  ResponseEntity<LoadResponse> load(@Valid @RequestBody LoadBatchRequest request){
        return ResponseEntity.ok(service.load(request));
    }


    @PatchMapping("/load/{batch_id}")
    public  ResponseEntity<LoadResponse> updateLoad(@RequestParam Integer batch_id,@Valid @RequestBody LoadBatchRequest request){
        return ResponseEntity.ok(service.updateLoad(batch_id,request));
    }


    @PostMapping("/availability")
    public ResponseEntity<List<DroneResponse>> availability(){
        return ResponseEntity.ok(service.checkAvailableDrones());
    }

    @GetMapping("/battery/{serial_number}")
    public ResponseEntity<DroneResponse> checkBattery(@RequestParam String serialNumber){
        return ResponseEntity.ok(service.checkBattery(serialNumber));
    }


}
