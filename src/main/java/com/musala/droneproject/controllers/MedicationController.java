package com.musala.droneproject.controllers;

import com.musala.droneproject.models.dtos.requests.IDDtos;
import com.musala.droneproject.models.dtos.requests.MedicationRequest;
import com.musala.droneproject.models.dtos.responses.MedicationResponse;
import com.musala.droneproject.services.MedicationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("v1/medication")
@Validated
@Slf4j
public class MedicationController  extends BaseController{
    private final MedicationService service;

    @Autowired
    public MedicationController(MedicationService service) {
        this.service = service;
    }


    @PostMapping("/create")
    public ResponseEntity<MedicationResponse> create(  @RequestBody MedicationRequest request){
       return ResponseEntity.ok(service.create(request));

    }



    @GetMapping("/all")
    public ResponseEntity<List<MedicationResponse>> GetAll(
        @RequestParam(value = "page_number", required = false, defaultValue = "0")    int page,
        @RequestParam(value = "page_size", required = false, defaultValue = "10") int size
    ){
        return ResponseEntity.ok(service.getAll(page,size));
    }

    @PostMapping("/get")
    public ResponseEntity<List<MedicationResponse>> getByCodes(@RequestBody IDDtos<String> codes){
        return ResponseEntity.ok(service.findAllByCode(codes.getIds()));
    }


    @GetMapping("/get/{code}")
    public ResponseEntity<MedicationResponse> getByCode(@RequestParam String code){
        return ResponseEntity.ok(service.findByCode(code));
    }
}
