package com.musala.droneproject.controllers;


import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class BaseController {
    ResponseEntity<ApiResponse> errorDispatch(ImmutablePair<HttpStatus, String> errorMessage){

        if(!errorMessage.getLeft().equals(HttpStatus.OK)){
            switch (errorMessage.getLeft()){
                case BAD_REQUEST:
                    return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                case CONFLICT:
                default:
                    return  new ResponseEntity<>(HttpStatus.CONFLICT);

            }
        }

        return null;
    }
}
