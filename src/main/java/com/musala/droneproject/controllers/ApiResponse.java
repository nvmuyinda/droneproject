package com.musala.droneproject.controllers;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiResponse<T> implements Serializable {

    private static final long serialVersionUID = 2606790555796441397L;
    private boolean status;
    private int successCode;
    private String successDescription;
    private int errorCode;
    private String errorDescription;
    private T data;

}
