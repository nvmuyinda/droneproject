package com.musala.droneproject.services;

import com.musala.droneproject.models.dtos.requests.MedicationRequest;
import com.musala.droneproject.models.dtos.responses.MedicationResponse;
import com.musala.droneproject.models.entities.Medication;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface MedicationService {


    MedicationResponse findByCode(String code);
    List<MedicationResponse> findAllByCode(List<String> codes);

    List<MedicationResponse> getAll(int page, int size);

    MedicationResponse create(MedicationRequest request);

    Medication populate(MedicationResponse response);
    MedicationResponse  populate(Medication entity);
    Medication populate(MedicationRequest request);
}
