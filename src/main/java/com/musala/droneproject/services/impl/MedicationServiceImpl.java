package com.musala.droneproject.services.impl;

import com.musala.droneproject.models.dtos.requests.MedicationRequest;
import com.musala.droneproject.models.dtos.responses.MedicationResponse;
import com.musala.droneproject.models.entities.Medication;
import com.musala.droneproject.repositories.MedicationRepository;
import com.musala.droneproject.services.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class MedicationServiceImpl implements MedicationService {

    private MedicationRepository medicationRepository;

    @Autowired
    public MedicationServiceImpl(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }



    /**
     * @param code
     */
    @Override
    public MedicationResponse findByCode(String code) {
       Optional<Medication> medication = medicationRepository.findByCode(code);
       if(medication.isEmpty()) return  null;
       return populate(medication.get());
    }

    /**
     * @param codes
     * @return
     */
    @Override
    public List<MedicationResponse> findAllByCode(List<String> codes) {
       return   (medicationRepository.findAllByCode(codes))
               .stream()
               .map(record->populate(record))
               .collect(Collectors.toList());
    }

    /**
     * @param page
     * @param size
     * @return
     */
    @Override
    public List<MedicationResponse> getAll(int page, int size) {
        Pageable pageable = PageRequest.of(page,size);
        return  medicationRepository.findAll(pageable).stream().map(x->populate(x)).collect(Collectors.toList());
    }

    /**
     * @param request
     * @return
     */
    @Override
    public MedicationResponse create(MedicationRequest request) {
      return populate( medicationRepository.save(populate(request)));
    }

    /**
     * @param response
     * @return
     */
    @Override
    public Medication populate(MedicationResponse response) {
        Medication  entity = new Medication();
        entity.setId(response.getId());
        entity.setCode(response.getCode());
        entity.setName(response.getName());
        entity.setImage(response.getImage());
        entity.setWeight(response.getWeight());
        entity.setCreatedAt(response.getCreatedAt());
        entity.setUpdatedAt(response.getUpdatedAt());
        return entity;
    }

    @Override
    public MedicationResponse   populate(Medication entity){

        MedicationResponse  response = new MedicationResponse();
        response.setId(entity.getId());
        response.setCode(entity.getCode());
        response.setName(entity.getName());
        response.setImage(entity.getImage());
        response.setWeight(entity.getWeight());
        response.setCreatedAt(entity.getCreatedAt());
        response.setUpdatedAt(entity.getUpdatedAt());
        return response;
    }
    @Override
    public Medication populate(MedicationRequest request){
        Medication entity = new Medication();
        entity.setCode(request.getCode());
        entity.setName(request.getName());
        entity.setWeight(request.getWeight());
        entity.setImage(request.getImage());
        return entity;
    }
}
