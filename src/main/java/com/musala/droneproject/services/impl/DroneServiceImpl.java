package com.musala.droneproject.services.impl;

import com.musala.droneproject.models.dtos.requests.DroneRequest;
import com.musala.droneproject.models.dtos.requests.LoadBatchRequest;
import com.musala.droneproject.models.dtos.requests.MedicationItemRequest;
import com.musala.droneproject.models.dtos.responses.DroneResponse;
import com.musala.droneproject.models.dtos.responses.LoadResponse;
import com.musala.droneproject.models.dtos.responses.MedicationResponse;
import com.musala.droneproject.models.entities.*;
import com.musala.droneproject.repositories.DroneBatteryAuditRepository;
import com.musala.droneproject.repositories.DroneRepository;
import com.musala.droneproject.repositories.LoadBatchRepository;
import com.musala.droneproject.services.DroneService;
import com.musala.droneproject.services.MedicationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Component
@Slf4j
public class DroneServiceImpl implements DroneService {


    public static final double MINIMUM_ACCEPTABLE_POWER_CAPACITY = 25d;
    private DroneRepository droneRepository;
    private MedicationService medicationService;
    private LoadBatchRepository loadBatchRepository;

    private DroneBatteryAuditRepository droneBatteryAuditRepository;
    @Autowired
    public DroneServiceImpl(DroneRepository droneRepository,
                            LoadBatchRepository loadBatchRepository,
                            DroneBatteryAuditRepository droneBatteryAuditRepository,
                            MedicationService medicationService) {
        this.droneRepository = droneRepository;
        this.loadBatchRepository = loadBatchRepository;
        this.droneBatteryAuditRepository = droneBatteryAuditRepository;
        this.medicationService =medicationService;
    }

    /**
     * @param request
     * @return
     */
    @Override
    public DroneResponse register(DroneRequest request) throws RuntimeException{
         return populate(droneRepository.save(populate(request)));
    }



    /**
     * @param request
     * @return
     */
    @Transactional
    @Override
    public LoadResponse load(LoadBatchRequest request) {
        log.info("Entering Loading Method");
        //task: validate drone exists..
        Optional<Drone> droneEntity =  droneRepository.findBySerialNumber(request.getDroneSerial());

        if(droneEntity.isEmpty()){
            log.error("Drone with serial %s does   Not  Exist",request.getDroneSerial());
            throw new RuntimeException(format("Drone with serial %s does   Not  Exist",request.getDroneSerial()));
        }
        Drone drone = droneEntity.get();;
        validateDrone(drone);

        //task: validate drugs only , probablly throw if some drug does not exist.
        if(request.getItems() == null){
            log.error("Batch has  no items added ");
            throw new RuntimeException(format("batch has  no items added"));
        }

        List<MedicationResponse>  medicationResponses =   medicationService.findAllByCode(request.getItems().stream().map(x->x.getCode()).collect(Collectors.toList()));

        if(medicationResponses.size() != request.getItems().size()){
            List<MedicationItemRequest> itemsMissing = request.getItems()
                    .stream()
                    .filter(requestItem -> medicationResponses.stream()
                            .anyMatch(existingItem ->
                                    existingItem.getCode() != requestItem.getCode()
                                    )
                    ).collect(Collectors.toList());

            log.error("Batch Items Added  do not  exist %s ",(itemsMissing.stream().map(x->x.getCode()).collect(Collectors.toList())) );
            throw new RuntimeException(format("Batch Items Added  do not  exist"));

        }

        validateLoadCappacity(drone, medicationResponses);


        log.error("Updating Drone Status to  Loading  ");
        {
            drone.setState(DroneState.LOADING);
            droneRepository.save(drone);
        }

        LoadBatch batch = new LoadBatch();
        batch.setDrone(droneEntity.get());
        List<LoadItem> loadItems = new ArrayList<>();
        for (MedicationResponse response: medicationResponses
             ) {
            loadItems.add( populate(batch, response) );
        }
        batch.setItems(loadItems);

        batch = loadBatchRepository.save(batch);

        log.info("Updating Drone Status to  Loaded  ");
        {
            drone.setState(DroneState.LOADED);
            droneRepository.save(drone);
        }
        //todo: save and return response.
        return populate(batch);
    }


    public LoadResponse updateLoad(Integer batch_id, LoadBatchRequest request){
       Optional<LoadBatch> batchEntity =  loadBatchRepository.findById(batch_id);
       if(batchEntity.isEmpty()){
           log.error("Record does not exist ");
           throw new RuntimeException(format("Record does not exist"));

       }
       LoadBatch batch =  batchEntity.get();
       Drone drone =batch.getDrone();
       if(drone.getState() != DroneState.LOADED  || drone.getState() != DroneState.LOADING){
           log.error("Drone current state does not allow adding items ");
           throw new RuntimeException(format("Drone current state does not allow adding items  {} ",drone.getState()));
       }

        List<MedicationResponse>  medicationResponses =   medicationService.findAllByCode(request.getItems().stream().map(x->x.getCode()).collect(Collectors.toList()));

        if(medicationResponses.size() != request.getItems().size()){
            List<MedicationItemRequest> itemsMissing = request.getItems()
                    .stream()
                    .filter(requestItem -> medicationResponses.stream()
                            .anyMatch(existingItem ->
                                    existingItem.getCode() != requestItem.getCode()
                            )
                    ).collect(Collectors.toList());

            log.error("Some  Batch Items added do not exist  ");
            throw new RuntimeException(format("Batch Items Added  do not  exist {} ",(itemsMissing.stream().map(x->x.getCode()).collect(Collectors.toList())) ));

        }

        validateLoadCappacity( drone, medicationResponses);

        log.error("Updating Drone Status to  Loading  ");
        {
            drone.setState(DroneState.LOADING);
            droneRepository.save(drone);
        }

        batch.setItems(null);
        List<LoadItem> loadItems = new ArrayList<>();
        for (MedicationResponse response: medicationResponses
        ) {
            loadItems.add(populate(batch, response));
        }
        batch.setItems(loadItems);

        batch = loadBatchRepository.save(batch);

        log.error("Updating Drone Status to  Loaded  ");
        {
            drone.setState(DroneState.LOADED);
            droneRepository.save(drone);
        }

        return  populate(batch);
    }

    /**
     *
     */

    @Override
    @Scheduled(cron = "2 * * * * *")
    @Transactional
    public void droneAudit() {
        droneBatteryAuditRepository.saveAll( droneRepository.findAll().stream().map(x->populateAud(x)).collect(Collectors.toList()));

    }

    public static void validateLoadCappacity(Drone drone, List<MedicationResponse> medicationResponses) {
        double totalload = medicationResponses.stream().map(x->x.getWeight()).reduce(0d,(x, y)->x+y);
        if(totalload > drone.getWeightLimit()){
            log.error("Total Load  is greater than load capacity  ");
            throw new RuntimeException(format("Total Load  is greater than load capacity"  ));
        }
    }

    private   void validateDrone(Drone drone) {
        //task:  vavlidate drone availabilityys
        if(!isDronnAvailable(drone)){
            log.error("Drone is not available for loading it is currently %s ", drone.getState());
            throw new RuntimeException(format("Drone is not available for loading it is currently %s ", drone.getState()));
        }
        //task: validate drone power
        if(!isDronePowerEnough(drone)){
            log.error("Drone  does not have enough power ,it is current %s ", drone.getBatteryCapacity());
            throw new RuntimeException(format("Drone  does not have enough power ,it is current %s ", drone.getBatteryCapacity()));
        }
    }


    public    boolean isDronePowerEnough(Drone entiiy){
        return  entiiy.getBatteryCapacity() >= MINIMUM_ACCEPTABLE_POWER_CAPACITY;
    }


    public    boolean isDronnAvailable(Drone entiiy){
        return  entiiy.getState() == DroneState.IDLE;
    }


    /**
     * @param
     * @return
     */
    @Override
    public List<DroneResponse> checkAvailableDrones() {
             return    (droneRepository.findAll())
                                     .stream()
                     .filter(x->isDronnAvailable(x) ==true)
                     .map(response->populate(response)).collect(Collectors.toList());

    }

    /**
     * @param serialNumber
     * @return
     */
    @Override
    public DroneResponse checkBattery(String serialNumber) {
        Optional<Drone> droneEntity =  droneRepository.findBySerialNumber(serialNumber);
        return droneEntity.isEmpty() ? null : populate(droneEntity.get());


    }

    private LoadResponse populate(LoadBatch batch) {
        LoadResponse response = new LoadResponse();
        response.setId(batch.getId());
        response.setDrone(populate(batch.getDrone()));
        response.setItems(batch.getItems().stream().map(x->medicationService.populate(x.getItem())).collect(Collectors.toList()));
        response.setCreatedAt(batch.getCreatedAt());
        response.setUpdatedAt(batch.getUpdatedAt());
        return response;
    }


    private DroneBatteryAudit populateAud(Drone entity) {
        DroneBatteryAudit response = new DroneBatteryAudit();
        response.setDrone(entity);
        response.setBatteryLevel(entity.getBatteryCapacity());

        return response;
    }

    private DroneResponse populate(Drone entity) {
        DroneResponse response = new DroneResponse();
        response.setId(entity.getId());
        response.setSerialNumber(entity.getSerialNumber());
        response.setModel(entity.getModel());
        response.setWeightLimit(entity.getWeightLimit());
        response.setBatteryCapacity(entity.getBatteryCapacity());
        response.setState(entity.getState());
        response.setCreatedAt(entity.getCreatedAt());
        response.setUpdatedAt(entity.getUpdatedAt());
        return response;
    }

    private Drone populate(DroneRequest request) {
        Drone entity = new Drone();
        entity.setSerialNumber(request.getSerialNumber());
        entity.setModel(request.getModel());
        entity.setWeightLimit(request.getWeightLimit());
        entity.setBatteryCapacity(request.getBatteryCapacity());
        entity.setState(request.getState() !=null ?request.getState() : DroneState.IDLE);

        return  entity;
    }

    private LoadItem populate(LoadBatch batch, MedicationResponse response) {
        LoadItem item = new LoadItem();
        item.setBatch(batch);
        item.setItem(medicationService.populate(response));
        return item;
    }


}
