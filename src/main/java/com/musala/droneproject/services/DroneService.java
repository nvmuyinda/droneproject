package com.musala.droneproject.services;

import com.musala.droneproject.models.dtos.requests.DroneRequest;
import com.musala.droneproject.models.dtos.requests.LoadBatchRequest;
import com.musala.droneproject.models.dtos.responses.DroneResponse;
import com.musala.droneproject.models.dtos.responses.LoadResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DroneService {
      DroneResponse register(DroneRequest request)throws RuntimeException;
      LoadResponse load(LoadBatchRequest request);
      List<DroneResponse> checkAvailableDrones();
      DroneResponse checkBattery(String serialNumber);

      LoadResponse updateLoad(Integer batch_id, LoadBatchRequest request);

        void droneAudit();
}
