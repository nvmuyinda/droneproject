package com.musala.droneproject.repositories;

import com.musala.droneproject.models.entities.LoadBatch;
import org.springframework.data.repository.CrudRepository;

public interface LoadBatchRepository extends CrudRepository<LoadBatch,Integer> {
}
