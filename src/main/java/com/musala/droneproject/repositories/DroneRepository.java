package com.musala.droneproject.repositories;

import com.musala.droneproject.models.entities.Drone;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DroneRepository extends CrudRepository<Drone,Integer> {

    Optional<Drone> findBySerialNumber(String serialNumber);

    @Override
    List<Drone> findAll();
 
}
