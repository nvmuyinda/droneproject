package com.musala.droneproject.repositories;

import com.musala.droneproject.models.entities.DroneBatteryAudit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DroneBatteryAuditRepository  extends CrudRepository<DroneBatteryAudit,Integer> {

}
