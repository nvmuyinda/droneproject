package com.musala.droneproject.repositories;

import com.musala.droneproject.models.entities.Medication;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MedicationRepository extends CrudRepository<Medication,Integer> {

    Optional<Medication> findByCode(String code);

    @Query("Select med from Medication med where code in :codes")
    List<Medication> findAllByCode(List<String> codes);

    Page<Medication> findAll(Pageable pageable);
}
