package com.musala.droneproject.models.dtos.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class MedicationRequest {

    @JsonProperty("name")
    @NotNull(message="name  is required")
    private String name;

    @JsonProperty("code")
    @NotNull(message="code   is required")
    private String code;

    @JsonProperty("weight")
    @NotNull(message="  Weight   is required")
    private double weight;

    @JsonProperty( "image")
    private String image;


}
