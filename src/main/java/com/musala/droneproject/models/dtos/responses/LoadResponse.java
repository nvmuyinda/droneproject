package com.musala.droneproject.models.dtos.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoadResponse {
    @JsonProperty("id")
    private Integer id;
    @JsonProperty( "drone")
    private DroneResponse drone;
    @JsonProperty( "items")
    private List<MedicationResponse> items;


    @JsonProperty( "created_at")
    private LocalDateTime createdAt;

    @JsonProperty( "updated_at")
    private LocalDateTime updatedAt;


}
