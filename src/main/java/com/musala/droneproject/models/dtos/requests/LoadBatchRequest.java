package com.musala.droneproject.models.dtos.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoadBatchRequest {

    @JsonProperty("drone_serial_number")
    @NotNull(message="Serial number is required")
    private String droneSerial;

    @JsonProperty("items")
    private List<MedicationItemRequest> items;

}
