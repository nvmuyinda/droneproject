package com.musala.droneproject.models.dtos.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.musala.droneproject.models.entities.DroneModel;
import com.musala.droneproject.models.entities.DroneState;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class DroneRequest {

    @JsonProperty("serialnumber")
    @NotNull(message="Serial number is required")
    private String serialNumber;

    @JsonProperty("model")
    @NotNull(message="Drone Model is required")
    private DroneModel model;

    @JsonProperty("weight")
    @NotNull(message="Drone Weight Limit is required")
    private double weightLimit;

    @JsonProperty("battery_capacity")
    @NotNull(message="provide battery capacity is required")
    private double batteryCapacity;

    @JsonProperty( "state")
    private DroneState state=DroneState.IDLE;


}
