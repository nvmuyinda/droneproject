package com.musala.droneproject.models.dtos.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class MedicationItemRequest {
    @JsonProperty("code")
    @NotNull(message="Medication code   is required")
    private String code;

}
