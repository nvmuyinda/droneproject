package com.musala.droneproject.models.dtos.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class MedicationResponse {


    @JsonProperty("id")
    private Integer id;


    @JsonProperty( "name" )
    private String name;

    @JsonProperty(  "weight")
    private double weight;

    @JsonProperty( "code")
    private String code;

    @JsonProperty(  "image")
    private String image;


    @JsonProperty( "created_at")
    private LocalDateTime createdAt;

    @JsonProperty( "updated_at")
    private LocalDateTime updatedAt;



}
