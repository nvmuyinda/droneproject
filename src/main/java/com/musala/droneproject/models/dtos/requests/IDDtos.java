package com.musala.droneproject.models.dtos.requests;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class IDDtos<T> {

    private List<T> ids;
}
