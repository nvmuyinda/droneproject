package com.musala.droneproject.models.dtos.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.musala.droneproject.models.entities.DroneModel;
import com.musala.droneproject.models.entities.DroneState;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class DroneResponse {

    @JsonProperty("id")
    private Integer id;


    @JsonProperty("serialnumber")
    private String serialNumber;

    @JsonProperty("model")
    private DroneModel model;

    @JsonProperty("weight_limit")
    private double weightLimit;

    @JsonProperty("battery_capacity")
    private double batteryCapacity;

    @JsonProperty( "state")
    private DroneState state;



    @JsonProperty( "created_at")
    private LocalDateTime createdAt;

    @JsonProperty( "updated_at")
    private LocalDateTime updatedAt;

}
