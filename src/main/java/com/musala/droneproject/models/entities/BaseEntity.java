package com.musala.droneproject.models.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@MappedSuperclass
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(
            name = "created_at",
            updatable = false
    )
    @CreationTimestamp
    @ColumnDefault("now()")

    private LocalDateTime createdAt;
    @Column(
            name = "updated_at"
    )
    @UpdateTimestamp
    @ColumnDefault("now()")
    private LocalDateTime updatedAt;


}
