package com.musala.droneproject.models.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "drones")
public class Drone extends BaseEntity  {
    @Column(name = "serialnumber",unique=true,length = 100)
    private String serialNumber;

    @Column(name = "model")
    @Enumerated(value = EnumType.STRING)
    private DroneModel model;

    @Column(name = "weight_limit")
    private double weightLimit;

    @Column(name = "battery_capacity")
    private double batteryCapacity;

    @Column(name = "state")
    @Enumerated(value = EnumType.STRING)
    private DroneState state=DroneState.IDLE;

}
