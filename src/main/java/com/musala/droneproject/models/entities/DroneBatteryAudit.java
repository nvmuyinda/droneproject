package com.musala.droneproject.models.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "dronebatteryaudit")
public class DroneBatteryAudit extends BaseEntity {
    @OneToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "drone_id",referencedColumnName = "id")
    private Drone drone;

    @Column(name = "battery_level")
    private double batteryLevel;

}
