package com.musala.droneproject.models.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "loadbatch")
public class LoadBatch extends BaseEntity{

    @OneToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "drone_id",referencedColumnName = "id")
    private Drone drone;


    @OneToMany(mappedBy="id",fetch=FetchType.LAZY,cascade = CascadeType.PERSIST)
    private List<LoadItem> items;


}
