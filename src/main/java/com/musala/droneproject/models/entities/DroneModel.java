package com.musala.droneproject.models.entities;

public enum DroneModel {
    Lightweight, Middleweight, Cruiserweight, Heavyweight;
}
