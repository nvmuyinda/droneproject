package com.musala.droneproject.models.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Embeddable
@Getter
@Setter
@Entity
@Table(name = "loaditems")
public class LoadItem extends BaseEntity{

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = LoadBatch.class)
    @JoinColumn(name = "batch_id",referencedColumnName = "id",nullable = false)
    private LoadBatch batch;

    @OneToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "item_id",referencedColumnName = "id")
   private Medication item;

}
