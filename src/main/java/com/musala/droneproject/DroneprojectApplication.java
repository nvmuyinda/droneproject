package com.musala.droneproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

//@EnableAsync
@SpringBootApplication(scanBasePackages = {"com.musala.droneproject"})
@EnableJpaRepositories(basePackages = {"com.musala.droneproject.repositories"})
@EntityScan(basePackages = {"com.musala.droneproject.models.entities"})
@EnableScheduling
public class DroneprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(DroneprojectApplication.class, args);
	}

}
