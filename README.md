# DRONE PROJECT
This  Micro Service simulates Drone Loading with Batches of Medication
items.

## Technology
This application is a restAPI built using JAVA   Spring boot.backed by <br/>
an in memory H2  database.

### Running the application

run command on terminal in the root directory  of the application <br/>
<code> .\gradlew bootRun </code>

### Running the Tests

<code> .\gradlew test </code> 

IDE used is Intelij

### Endpoints
The application uses swagger for endpoint visuaization <br/>
<code> http://localhost:8080/swagger-ui/ </code>

### database

The System is using in memory H2 database. to access the console <br/>
<code>http://localhost:8080/h2-console</code> <br/>


JDBC URI : jdbc:h2:mem:droneapp
<table>
<tr>
    <th>Username</th>
<th>Password</th>
</tr>

<tr>
    <td>sa</td>
    <td>password</td>
</tr>
</table>

